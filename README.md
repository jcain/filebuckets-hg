# FileBuckets HG

FileBuckets HG is a library of file bucket interfaces and implementations for PHP 7.2+.


## Installation

```batchfile
composer require jcain/filebuckets-hg
```


## Component stability statuses

| Component                                                           | Stability | Since |
|:--------------------------------------------------------------------|:---------:|:-----:|
| [FileBucket](src/FileBucket.api.md)                                 | alpha     | 0.0   |
| [FileBucketFile](src/FileBucketFile.api.md)                         | alpha     | 0.0   |
| [Util](src/Util.api.md)                                             | alpha     | 0.0   |
| [Basic/FileBucketWrapper](src/Basic/FileBucketWrapper.api.md)       | alpha     | 0.0   |
| [Basic/FileSystemFileBucket](src/Basic/FileSystemFileBucket.api.md) | alpha     | 0.0   |
| [Basic/MemoryFileBucket](src/MemoryFileBucket.api.md)               | alpha     | 0.0   |

The **Stability** column indicates the component's stability status, and the **Since** column indicates the package version when the component first achieved that stability.

Each component and its members has a **stability status** indicating how stable the interface and implementation is to depend on in production. The stability may be one of the following:

* **alpha**: The interface and implementation are unstable and may change significantly.
* **beta**: The interface is stable but its implementation is not sufficiently tested.
* **omega**: The interface and implementation are stable and considered ready for production use.

A component's stability status is the same as the highest stability status of its members. Once a member's stability is raised, it will not be reduced.


## License

FileBuckets HG is licensed under the MIT license. See the [LICENSE](LICENSE.md) file for more information.