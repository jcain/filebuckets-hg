<?php namespace JCain\FileBuckets\HG;


class Util {
	static public function iso8601(int $time) : string {
		return gmdate('Y-m-d\TH:i:s\Z', $time);
	}
}