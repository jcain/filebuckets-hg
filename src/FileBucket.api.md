# JCain\FileBuckets\HG\FileBucket


## FileBucket interface

_Stability: **alpha**, Since: **0.0**_

```php
interface FileBucket {
  // Methods
  function list($prefix : string = '') : Iterator
  function file($name : string) : FileBucketFile
  function exists($name : string) : bool
  function create($name : string, $data : mixed, $meta : array = null) : FileBucketFile
  function delete($name : string) : void
  function getMeta($name : string) : ?array
  function getData($name : string) : ?string
  function getDataStream($name : string) : resource
  function getUrl($name : string) : ?string
}
```


### Methods


#### list()

```php
function list($prefix : string = '') : Iterator
```


#### file()

```php
function file($name : string) : FileBucketFile
```


#### exists()

```php
function exists($name : string) : bool
```


#### create()

```php
function create($name : string, $data : mixed, $meta : array = null) : FileBucketFile
```


#### delete()

```php
function delete($name : string) : void
```


#### getMeta()

```php
function getMeta($name : string) : ?array
```


#### getData()

```php
function getData($name : string) : ?string
```


#### getDataStream()

```php
function getDataStream($name : string) : resource
```


#### getUrl()

```php
function getUrl($name : string) : ?string
```