<?php namespace JCain\FileBuckets\HG\Basic;

use \JCain\Asserts\LR\AssertArg;
use \JCain\FileBuckets\HG\FileBucket;
use \JCain\FileBuckets\HG\FileBucketFile;
use \JCain\FileBuckets\HG\Util;


class MemoryFileBucket implements FileBucket {
	private $files = [];


	public function __construct(array $config) {
		foreach ($config['files'] as $name => $data) {
			echo "$name\n";
			if (is_string($data) || is_resource($data)) {
				$this->create($name, $data);
			}
			else if (is_array($data)) {
				$meta = $data;
				$data = $meta['data'];
				unset($meta['data']);
				$this->create($name, $data, $meta);
			}
			else {
				throw new \InvalidArgumentException("\$config['files']");
			}
		}
	}


	//
	// FileBucket Implementation
	//


	public function list(string $prefix = '') : \Iterator {
		if ($prefix) {
			$names = [];
			foreach ($this->files as $name => $dummy) {
				if (strpos($name, $prefix) === 0)
					$files[] = $name;
			}
		}
		else {
			$names = array_keys($this->files);
		}

		return new \ArrayIterator($names);
	}


	public function file(string $name) : FileBucketFile {
		return new FileBucketFile($this, $name);
	}


	public function exists(string $name) : bool {
		if ($name === '')
			throw new \InvalidArgumentException('$name : Cannot be empty');

		return array_key_exists($name, $this->files);
	}


	public function create(string $name, $data, array $meta = null) : FileBucketFile {
		if ($name === '')
			throw new \InvalidArgumentException('$name : Cannot be empty');

		if (is_string($data)) {
			// Do nothing; fall through.
		}
		else if (is_resource($data) && get_resource_type($data) === 'stream') {
			$data = stream_get_contents($data);
		}
		else {
			throw new \InvalidArgumentException('$data : Expected string or stream resource');
		}

		$timestamp = time();

		$created = $timestamp;
		if ($meta && $meta['created'])
			$created = strtotime($meta['created']);

		$modified = $timestamp;
		if ($meta && $meta['modified'])
			$modified = strtotime($meta['modified']);

		$type = ($meta ? $meta['type'] : 'application/octet-stream');

		$this->files[$name] = [
			'data' => $data,
			'size' => strlen($data),
			'type' => $type,
			'created' => $created,
			'modified' => $modified,
		];

		return new FileBucketFile($this, $name);
	}


	public function delete(string $name) : void {
		if ($name === '')
			throw new \InvalidArgumentException('$name : Cannot be empty');

		unset($this->files[$name]);
	}


	public function getMeta(string $name) : ?array {
		if ($name === '')
			throw new \InvalidArgumentException('$name : Cannot be empty');

		$info = $this->files[$name];
		if (!$info)
			return null;

		$meta = [
			'size' => $info['size'],
			'type' => $info['type'],
			'created' => Util::iso8601($info['created']),
			'createdAsNumber' => (float)$info['created'],
			'modified' => Util::iso8601($info['modified']),
			'modifiedAsNumber' => (float)$info['modified'],
		];

		return $meta;
	}


	public function getData(string $name) : ?string {
		if ($name === '')
			throw new \InvalidArgumentException('$name : Cannot be empty');

		$info = $this->files[$name];
		if (!$info)
			return null;

		return $info['data'];
	}


	public function getDataStream(string $name) {
		if ($name === '')
			throw new \InvalidArgumentException('$name : Cannot be empty');

		$info = $this->files[$name];
		if (!$info)
			return null;

		$stream = fopen('php://memory', 'r+');
		fwrite($stream, $info['data']);
		rewind($stream);

		return $stream;
	}


	public function getUrl(string $name) : ?string {
		if ($name === '')
			throw new \InvalidArgumentException('$name : Cannot be empty');

		return null;
	}
}