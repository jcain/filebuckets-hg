<?php namespace JCain\FileBuckets\HG\Basic;

use \JCain\Asserts\LR\AssertArg;
use \JCain\FileBuckets\HG\FileBucket;
use \JCain\FileBuckets\HG\FileBucketFile;
use \JCain\FileBuckets\HG\Util;


class FileSystemFileBucket implements FileBucket {
	private $baseDir;


	public function __construct(array $config) {
		$this->baseDir = AssertArg::isString($config['baseDir'], "\$config['baseDir']");
	}


	//
	// Methods
	//


	public function baseDir() : string {
		return $this->baseDir;
	}


	private function scandirDeep(&$files, $prefix, $prefixLen, $subdir) : void {
		$fullDirPath = "$this->baseDir/$subdir";

		foreach (scandir($fullDirPath) as $file) {
			if ($file === '.' || $file === '..')
				continue;

			$filePath = ($subdir ? "$subdir/$file" : $file);

			if (is_dir("$this->baseDir/$filePath")) {
				if (!$prefixLen || strlen($filePath) < $prefixLen && strpos($prefix, $filePath) === 0 || strpos($filePath, $prefix) === 0)
					$this->scandirDeep($files, $prefix, $prefixLen, $filePath);
			}
			else {
				if (!$prefixLen || strpos($filePath, $prefix) === 0)
					$files[] = $filePath;
			}
		}
	}


	//
	// FileBucket Implementation
	//


	public function list(string $prefix = '') : \Iterator {
		if (!is_dir($this->baseDir))
			return new \EmptyIterator();

		$files = [];
		$this->scandirDeep($files, $prefix, strlen($prefix), '');
		return new \ArrayIterator($files);
	}


	public function file(string $name) : FileBucketFile {
		return new FileBucketFile($this, $name);
	}


	public function exists(string $name) : bool {
		if ($name === '')
			throw new \InvalidArgumentException('$name : Cannot be empty');

		$fullPath = "$this->baseDir/$name";

		return is_file($fullPath);
	}


	public function create(string $name, $data, array $meta = null) : FileBucketFile {
		if ($name === '')
			throw new \InvalidArgumentException('$name : Cannot be empty');

		$fullPath = "$this->baseDir/$name";

		// Write the data to file.
		$result = @file_put_contents($fullPath, $data);
		if ($result === false) {
			@mkdir(dirname($fullPath), 0777, true);
			$result = @file_put_contents($fullPath, $data);
			if ($result === false)
				throw new \Exception("Failed writing to file '$fullPath'");
		}

		if ($meta && $meta['modified']) {
			$time = strtotime($meta['modified']);
			touch($fullPath, $time);
		}

		return new FileBucketFile($this, $name);
	}


	public function delete(string $name) : void {
		if ($name === '')
			throw new \InvalidArgumentException('$name : Cannot be empty');

		$fullPath = "$this->baseDir/$name";

		@unlink($fullPath);
	}


	public function getMeta(string $name) : ?array {
		if ($name === '')
			throw new \InvalidArgumentException('$name : Cannot be empty');

		$fullPath = "$this->baseDir/$name";

		$info = @stat($fullPath);
		if ($info === false)
			return null;

		$meta = [
			'size' => $info['size'],
			'type' => 'application/octet-stream',
			'created' => Util::iso8601($info['ctime']),
			'createdAsNumber' => (float)$info['ctime'],
			'modified' => Util::iso8601($info['mtime']),
			'modifiedAsNumber' => (float)$info['mtime'],
		];

		return $meta;
	}


	public function getData(string $name) : ?string {
		if ($name === '')
			throw new \InvalidArgumentException('$name : Cannot be empty');

		$fullPath = "$this->baseDir/$name";

		$data = @file_get_contents($fullPath);
		return ($data !== false ? $data : null);
	}


	public function getDataStream(string $name) {
		if ($name === '')
			throw new \InvalidArgumentException('$name : Cannot be empty');

		$fullPath = "$this->baseDir/$name";

		$handle = @fopen($fullPath, 'r');
		return ($handle !== false ? $handle : null);
	}


	public function getUrl(string $name) : ?string {
		if ($name === '')
			throw new \InvalidArgumentException('$name : Cannot be empty');

		return null;
	}
}