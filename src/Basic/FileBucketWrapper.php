<?php namespace JCain\FileBuckets\HG\Basic;

use \JCain\FileBuckets\HG\FileBucket;
use \JCain\FileBuckets\HG\FileBucketFile;


class FileBucketWrapper implements FileBucket {
	private $wrapped;


	public function __construct(FileBucket $wrapped) {
		$this->wrapped = $wrapped;
	}


	//
	// Methods
	//


	public function wrapped() : FileBucket {
		return $this->wrapped;
	}


	//
	// FileBucket Implementation
	//


	public function list(string $prefix = '') : \Iterator {
		return $this->wrapped->list($prefix);
	}


	public function file(string $name) : FileBucketFile {
		return $this->wrapped->file($name);
	}


	public function exists(string $name) : bool {
		return $this->wrapped->exists($name);
	}


	public function create(string $name, $data, array $meta = null) : FileBucketFile {
		return $this->wrapped->create($name, $data, $meta);
	}


	public function delete(string $name) : void {
		$this->wrapped->delete($name);
	}


	public function getMeta(string $name) : ?array {
		return $this->wrapped->getMeta($name);
	}


	public function getData(string $name) : ?string {
		return $this->wrapped->getData($name);
	}


	public function getDataStream(string $name) {
		return $this->wrapped->getDataStream($name);
	}


	public function getUrl(string $name) : ?string {
		return $this->wrapped->getUrl($name);
	}
}