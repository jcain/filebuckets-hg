# JCain\FileBuckets\HG\Util


## Util class

_Stability: **alpha**, Since: **0.0**_

```php
class Util {
  // Static Methods
  static public function iso8601($time : int) : string
}
```


### Methods


#### iso8601()

```php
static public function iso8601($time : int) : string
```