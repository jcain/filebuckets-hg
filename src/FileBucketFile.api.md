# JCain\FileBuckets\HG\FileBucketFile


## FileBucketFile class

_Stability: **alpha**, Since: **0.0**_

```php
class FileBucketFile {
  // Methods
  public function exists() : bool
  public function create($data : mixed, $meta : array = null) : array
  public function delete() : void
  public function getMeta() : ?array
  public function getData() : ?string
  public function getDataStream() : resource
  public function getUrl() : ?string
}
```


### Methods


#### exists()

```php
function exists() : bool
```


#### create()

```php
public function create($data : mixed, $meta : array = null) : array
```


#### delete()

```php
public function delete() : void
```


#### getMeta()

```php
public function getMeta() : ?array
```


#### getData()

```php
public function getData() : ?string
```


#### getDataStream()

```php
public function getDataStream() : resource
```


#### getUrl()

```php
public function getUrl() : ?string
```