<?php namespace JCain\FileBuckets\HG;


class FileBucketFile {
	private $bucket;
	private $name;


	public function __construct(FileBucket $bucket, string $name) {
		$this->bucket = $bucket;
		$this->name = $name;
	}


	//
	// Methods
	//


	public function bucket() : FileBucket {
		return $this->bucket;
	}


	public function name() : string {
		return $this->name;
	}


	public function exists() : bool {
		return $this->bucket->exists($this->name);
	}


	public function create($data, array $meta = null) : array {
		return $this->bucket->create($this->name, $data, $meta)->getMeta();
	}


	public function delete() : void {
		$this->bucket->delete($this->name);
	}


	public function getMeta() : ?array {
		return $this->bucket->getMeta($this->name);
	}


	public function getData() : ?string {
		return $this->bucket->getData($this->name);
	}


	public function getDataStream() {
		return $this->bucket->getDataStream($this->name);
	}


	public function getUrl() : ?string {
		return $this->bucket->getUrl($this->name);
	}


	public function __toString() {
		return "File[$this->name]";
	}
}