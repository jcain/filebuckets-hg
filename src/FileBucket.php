<?php namespace JCain\FileBuckets\HG;


interface FileBucket {
	function list(string $prefix = '') : \Iterator;


	function file(string $name) : FileBucketFile;


	function exists(string $name) : bool;


	function create(string $name, $data, array $meta = null) : FileBucketFile;


	function delete(string $name) : void;


	function getMeta(string $name) : ?array;


	function getData(string $name) : ?string;


	function getDataStream(string $name);


	function getUrl(string $name) : ?string;
}